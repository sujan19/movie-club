import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const API_BASE_URL = environment.movieApiUrl;

@Injectable()
export class HttpService {
  constructor(
    private http: HttpClient
  ) { }

  createHeader(headers: HttpHeaders) {
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('requestsource', 'localhost');
    return headers;
  }

  get(url) {
    let headers = new HttpHeaders();
    headers = this.createHeader(headers);
    return this.http.get(API_BASE_URL + url, {
      // headers: headers
    });
  }

  post(url, data) {
    let headers = new HttpHeaders();
    headers = this.createHeader(headers);
    return this.http.post(API_BASE_URL + url, data, {
      headers: headers
    });
  }
  postFormData(url, data) {
    let headers = new HttpHeaders();
    headers = headers.append('requestsource', 'localhost');
    return this.http.post(API_BASE_URL + url, data, {
      headers: headers
    });
  }

  put(url, data) {
    let headers = new HttpHeaders();
    headers = this.createHeader(headers);
    return this.http.put(API_BASE_URL + url, data, {
      headers: headers
    });
  }

  delete(url, data) {
    let headers = new HttpHeaders();
    headers = this.createHeader(headers);
    return this.http.put(API_BASE_URL + url, data, {
      headers: headers
    });
  }

  remove(url){
    let headers = new HttpHeaders();
    headers = this.createHeader(headers);
    return this.http.delete(API_BASE_URL + url, {
      headers: headers
    });
  }
}
