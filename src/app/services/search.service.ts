import { Injectable } from '@angular/core';
import { HttpService } from './http-client.service';
import { map } from 'rxjs/operators';
import { MovieType } from '../constants/index';
import { of } from 'rxjs';

@Injectable()
export class SearchService {
  constructor(
    private http: HttpService
  ) { }

  fetchMovieByTitle(term, type?) {
    if (term && term.length > 2) {
      return this.http.get(`&s=${term}&type=${type}`)
        .pipe(map((response:any) => {
          if(response.Response == "True"){
          return response.Search;
          }else{
            return [];
          }
        }));
    } else {
      return of([]);
    }
  }

  fetchMovieByPage(term, page, type?) {
    if (term && term.length > 2) {
      return this.http.get(`&s=${term}&page=${page}&type=${type}`)
        .pipe(map((response:any) => {
          return response;
        }));
    } else {
      return of([]);
    }
  }

  fetchMovieDetailById(id){
    return this.http.get(`&i=${id}`).pipe(map(response => {
      if (response) {
        return response;
      }
    }));
  }

  getMovieType(term){
    if (term && term.length > 1) {
      const movieList = MovieType.filter(
        res => res.Title.toLowerCase().indexOf(term.toLowerCase()) > -1);
      const uniqueList = [];
      movieList.filter(item => {
        const i = uniqueList.findIndex(x => ((`${x.Title}`) === (`${item.Title}`)));
        if (i <= -1) {
          uniqueList.push(item);
        }
      });
      return of(uniqueList);
    } else {
      return of([]);
    }
  }
}



