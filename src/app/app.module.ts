import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ROUTING } from './app.routing';
import { DatePipe } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { NgxPaginationModule } from 'ngx-pagination';

import {
  MDBBootstrapModule,
  ModalModule,
  NavbarModule,
  CollapseModule
} from 'angular-bootstrap-md';
import * as components from './components/index';
import * as common from './common/index';
import * as services from './services';
import * as pipes from './pipes/index';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser, TitleCasePipe } from '@angular/common';
import { OrderModule } from 'ngx-order-pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import * as utils from './utils/index';

@NgModule({
  declarations: [
    AppComponent,
    common.LoaderComponent,
    common.FamHeaderComponent,
    components.MovieDetailComponent,
    components.SearchbarComponent,
    components.FamLayoutComponent,
    components.MovieListComponent,
    pipes.FormatPhoneNumberPipe,
    pipes.ArraySortPipe,
    pipes.ManualFilterPipe,
    pipes.TruncatePipe,
    pipes.SafeHtmlPipe,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    NgSelectModule,
    NgbModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      toastClass: 'toast toast-bootstrap-compatibility-fix',
      timeOut: 2000,
      positionClass: 'toast-top-right'
    }),
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTING, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      useHash: false
      // onSameUrlNavigation: 'reload'
    }),
    HttpClientModule,
    MDBBootstrapModule.forRoot(),

    ModalModule,
    NavbarModule,
    NgSelectModule,
    CollapseModule,
    // ,MatProgressSpinnerModule
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    OrderModule,
    // NgxStripeModule.forRoot('pk_test_PVCDHvvNNKJKXZA2QmDNFO1V002Vn0jD6V'),
    NgOptionHighlightModule,
    InfiniteScrollModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    DatePipe,
    services.HttpService,
    services.SearchService,
    TitleCasePipe,
    utils.FamUtils,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string
  ) {
    const platform = isPlatformBrowser(platformId)
      ? 'in the browser'
      : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
