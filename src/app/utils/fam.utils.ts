import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { SearchService } from '../services/search.service';

@Injectable({
  providedIn: 'root'
})

export class FamUtils {
  constructor(
    @Inject(PLATFORM_ID) private platformId
    , private _searchService: SearchService
  ) {

  }
  public isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

}
