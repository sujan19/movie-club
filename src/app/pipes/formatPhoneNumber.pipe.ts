import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formatPhoneNumber' })
export class FormatPhoneNumberPipe implements PipeTransform {
  transform(number: number) {
    const s = ('' + number).replace(/\D/g, '');
    const m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
    return (!m) ? null : '(' + m[1] + ') ' + m[2] + '-' + m[3];
  }
}
