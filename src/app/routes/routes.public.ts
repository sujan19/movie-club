import { Routes } from '@angular/router';
import {
   MovieDetailComponent, FamLayoutComponent, MovieListComponent,
} from '../components';

export const RoutePublic: Routes = [
  { path: '', component: FamLayoutComponent, children: [{ path: '', component: MovieListComponent }] },
  { path: ':imdbID', component: MovieDetailComponent},
];


