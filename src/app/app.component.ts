import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FamUtils } from './utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isBrowser: boolean;

  constructor(
     private router: Router
    , private _utils: FamUtils
  ) {
    this.isBrowser = this._utils.isBrowser();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.isBrowser) {
          // (<any>window).ga('set', 'page', event.urlAfterRedirects);
          // (<any>window).ga('send', 'pageview');
        }
      }
    });
  }

  ngOnInit() {
  }
}
