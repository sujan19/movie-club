import {
  Component,
  OnInit,
  HostListener,
} from '@angular/core';

import { FamUtils } from '../../utils';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html'
})
export class MovieListComponent implements OnInit {
  fromFad;
  constructor(
    private _utils: FamUtils,) {
    this.isBrowser = this._utils.isBrowser();
    // this._seoService.setTitle('The fastest search for a doctor near you.');
    this.fromFad = true;
  }

  isBrowser: boolean;
  goTopArrowShown = false;

  
  @HostListener('window:scroll', []) onWindowScroll() {
    if (this.isBrowser) {
      if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
      ) {
        this.goTopArrowShown = true;
      } else {
        this.goTopArrowShown = false;
      }
    }
  }
  ngOnInit() {

  }

  goToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.goTopArrowShown = false;
  }
}
