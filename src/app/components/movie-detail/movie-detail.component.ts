import { Location } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../services';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  loading = false;
  locationPath: any;
  imdbId: any;
  movieDetail: any;
  @HostListener('window:scroll', []) onWindowScroll() {
    // sticky filter
    const element = document.getElementById('sidebar');
    if (window.pageYOffset > 120) {
      element.classList.add('sticky');
    } else if (element) {
      element.classList.remove('sticky');
    }
  }
  constructor(
    private _router: Router,
    private _searchService: SearchService,
    private _location: Location,
  ) { }
  ngOnInit() {
    this.locationPath = this._location.path().split('/');
    if ((this.locationPath[1])) {
      this.imdbId = this.locationPath[1];
    }
    this.loading = true;
    this._searchService.fetchMovieDetailById(this.imdbId).subscribe((res: any) => {
      this.loading = false;
      if (res.Response == "True") {
        this.movieDetail = res
      }
    })
  }
  homePage() {
    this._router.navigate(['']);
  }
}
