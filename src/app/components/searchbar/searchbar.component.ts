import {
  Component,
  OnInit,
  AfterContentChecked
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SearchService } from '../../services';
import { FamUtils } from '../../utils';
import {
  distinctUntilChanged,
  debounceTime,
  tap,
  switchMap,
  catchError,
  map
} from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import {
  FormControl,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html'
})
export class SearchbarComponent implements OnInit, AfterContentChecked {
  isBrowser: boolean;
  loading = false;
  showSearch = false;
  locationPath: any;
  bannerCollapse = false;
  bannerCollapseInput = false;
  providerSearch = false;
  searchFailed: boolean;
  bundleSearch: boolean;
  headElements = [
    { label: 'Title', value: 'Title' },
    { label: 'Year', value: 'Year' },
    { label: 'Type', value: 'Type' },
  ];
  label = 'Year';
  value = true;
  movie;
  type;
  p: number = 1;
  movieList = [];
  totalItem: number;
  searchForm: any;
  isSubmitted: boolean = false;
  constructor(
    private _router: Router
    , private _toastrService: ToastrService
    , private _searchService: SearchService
    , private _utils: FamUtils
    , private fb: FormBuilder
  ) {
    this.isBrowser = this._utils.isBrowser();
    this.setSearchForm();
  }
  ngOnInit() {
  }
  setSearchForm() {
    this.searchForm = this.fb.group({
      movie: new FormControl(''),
      type: new FormControl('')
    })
  }
  toggle(label) {
    this.label = label;
    this.value = !this.value;
  }
  ngAfterContentChecked() {
    this.bannerCollapseInput = this.bannerCollapse;
  }

  searchToggle() {
    this.showSearch = !this.showSearch;
  }

  submitSearchKeyword() {
    this.isSubmitted = true
    this.loading = true;
    this.p = 1;
    let type = typeof this.searchForm.value.type
    if (type == "object") {
      this.searchForm.value.type = this.searchForm.value.type.Title;
    }
    if (this.searchForm.value.movie && this.searchForm.value.type) {
      this.bannerCollapse = true;
      this.fetchMovieList(this.searchForm.value.movie, this.p, this.searchForm.value.type);
    } else if (this.searchForm.value.movie) {
      this.bannerCollapse = true;
      this.fetchMovieList(this.searchForm.value.movie, this.p, '');
    } else {
      this.loading = false;
      this.movieList = [];
      this._toastrService.warning(
        'Please enter the name'
      );
    }
  }
  fetchMovieList(movie, page, type) {
    this._searchService.fetchMovieByPage(movie, page, type).subscribe((res) => {
      this.loading = false;
      if (res.Response == "True") {
        this.movieList = res.Search;
        this.totalItem = res.totalResults;
      } else {
        this.movieList = []
      }
    })
  }
  goToPage(page) {
    this.p = page;
    this.type = this.type ? this.type : '';
    this.fetchMovieList(this.searchForm.value.movie, page, this.searchForm.value.type);
    if (this.isBrowser) {
      document.getElementById('el2_search').scrollIntoView({ behavior: 'smooth' });
    }
  }
  goToDetailPage(movie) {
    this._router.navigate([`/${movie.imdbID}`]);
  }
  searchMovie = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      tap(() => this.providerSearch
        = true),
      switchMap(term => {
        if (term.length > 2) {
          return this._searchService.fetchMovieByTitle(term, '').pipe(map(a => {
            this.providerSearch = false;
            return a;
          }
          ),
            tap(() => this.searchFailed = false),
            catchError(() => {
              this.searchFailed = true;
              this.providerSearch = false;
              return of([]);
            }));
        } else {
          this.providerSearch = false;
          return of([]);
        }
      }
      ),
      tap(() => this.providerSearch = false)
    )
  searchType = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      tap(() => this.providerSearch
        = true),
      switchMap(term => {
        if (term.length > 2) {
          return this._searchService.getMovieType(term).pipe(map(a => {
            this.providerSearch = false;
            return a;
          }
          ),
            tap(() => this.searchFailed = false),
            catchError(() => {
              this.searchFailed = true;
              this.providerSearch = false;
              return of([]);
            }));
        } else {
          this.providerSearch = false;
          return of([]);
        }
      }
      ),
      tap(() => this.providerSearch = false)
    )

  inputFormatterForProvider = (x: { Title: string }) => x.Title;

  selectItem(value, input) {
    if (input == 'typeSearch') {
      this.searchForm.patchValue({
        type: value.item.Title
      })
    } else {
      this.bannerCollapse = true;
      this._router.navigate([`/${value.item.imdbID}`]);
    }
  }
}
