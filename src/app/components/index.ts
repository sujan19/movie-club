export * from './layouts/index';
export * from './movie-detail/movie-detail.component';
export * from './searchbar/searchbar.component';
export * from './movie-list/movie-list.component';
